#!/usr/bin/env ruby
# frozen_string_literal: true

input_file_name = 'AGEEML_202054956843.csv'
output_file_name = 'localidades.sql'

require_relative 'parser'

LIMIT = 10000

File.open(output_file_name, 'w') do |output|
  output << Parser.table_definition

  accum = +Parser.insert_head
  counter = 0

  File.readlines(input_file_name).each_with_index do |line, index|
    next if index == 0

    accum << ', ' if counter > 0
    accum << Parser.line_to_sql(line)
    counter += 1

    next unless counter == LIMIT

    output << accum
    output << ';'
    accum = +Parser.insert_head
    counter = 0
  end

  output << accum
  output << ';'
end
