# frozen_string_literal: true

module Parser
  def self.table_definition
    %{DROP TABLE IF EXISTS localidad;
    CREATE TABLE localidad (
       id INT PRIMARY KEY AUTO_INCREMENT,
       cve_estado INT(2) NOT NULL,
       nom_estado VARCHAR(31) NOT NULL,
       cve_municipio INT(3) NOT NULL,
       nom_municipio VARCHAR(100) NOT NULL,
       cve_localidad INT(4) NOT NULL,
       nom_localidad VARCHAR(100) NOT NULL
     );

}
  end

  def self.insert_head()

    'insert into localidad('\
       'cve_estado,'\
       'nom_estado,'\
       'cve_municipio,'\
       'nom_municipio,'\
       'cve_localidad,'\
       'nom_localidad)'\
       'values '
  end

  def self.line_to_sql(line)
    chunks = line.split(/",?"/)

    "(#{chunks[1]}, \"#{chunks[2]}\", #{chunks[4]},"\
   "\"#{chunks[5]}\", #{chunks[6]}, \"#{chunks[7]}\")"
  end
end
